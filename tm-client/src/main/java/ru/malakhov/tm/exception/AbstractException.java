package ru.malakhov.tm.exception;

import org.jetbrains.annotations.NotNull;

public abstract class AbstractException extends Exception {

    public AbstractException() {
    }

    public AbstractException(@NotNull final String message) {
        super(message);
    }

    public AbstractException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

}