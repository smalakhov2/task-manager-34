package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.endpoint.SessionDto;

public interface IPropertyRepository {

    @Nullable
    SessionDto getSession();

    void setSession(@Nullable SessionDto session);

}