package ru.malakhov.tm.listener.admin.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.AdminDataEndpoint;
import ru.malakhov.tm.endpoint.ServerDto;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;
import ru.malakhov.tm.service.PropertyService;

@Component
public class ServerInfoShowListener extends AbstractListener {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private AdminDataEndpoint adminDataEndpoint;

    @NotNull
    @Override
    public String name() {
        return "server-info";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show server info.";
    }

    @Override
    @EventListener(condition = "@serverInfoShowListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws AbstractException_Exception {
        @Nullable final SessionDto session = propertyService.getSession();
        @NotNull final ServerDto server = adminDataEndpoint.getServerInfo(session);
        System.out.println("[SERVER-INFO]");
        System.out.println("SERVER HOST: " + server.getHost());
        System.out.println("SERVER PORT: " + server.getPort());
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
