package ru.malakhov.tm.listener.task;

import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.endpoint.TaskDto;
import ru.malakhov.tm.listener.AbstractListener;

public abstract class AbstractTaskShowListener extends AbstractListener {

    protected void showTask(@Nullable final TaskDto task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

}
