package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.AbstractDataTest;
import ru.malakhov.tm.api.service.ISessionService;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.dto.UserDto;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.empty.EmptyLoginException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;
import ru.malakhov.tm.exception.unknown.UnknownUserException;
import ru.malakhov.tm.exception.user.AccessDeniedException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class SessionServiceTest extends AbstractDataTest {

    @NotNull
    private final ISessionService sessionService = context.getBean(SessionService.class);

    @NotNull
    private final IUserService userService = context.getBean(UserService.class);

    @NotNull
    private final SessionDto sessionOne = sessionService.setSignature(
            new SessionDto(
                    System.currentTimeMillis(),
                    userDto.getId(),
                    null
            )
    );

    @NotNull
    private final SessionDto sessionTwo = sessionService.setSignature(
            new SessionDto(
                    System.currentTimeMillis(),
                    userDto.getId(),
                    null)
    );

    @NotNull
    private final SessionDto sessionThree = sessionService.setSignature(
            new SessionDto(
                    System.currentTimeMillis(),
                    adminDto.getId(),
                    null)
    );

    @NotNull
    private final SessionDto sessionFour = sessionService.setSignature(
            new SessionDto(
                    System.currentTimeMillis(),
                    adminDto.getId(),
                    null)
    );

    @NotNull
    private final SessionDto unknownSession = sessionService.setSignature(
            new SessionDto(System.currentTimeMillis(),
                    unknownUserDto.getId(),
                    null)
    );

    @NotNull
    private final List<SessionDto> userSessions = new ArrayList<>(Arrays.asList(sessionOne, sessionTwo));

    @NotNull
    private final List<SessionDto> adminSessions = new ArrayList<>(Arrays.asList(sessionThree, sessionFour));

    public SessionServiceTest() throws Exception {
        super();
    }

    private void loadData() {
        sessionService.saveAll(userSessions);
        sessionService.saveAll(adminSessions);
    }

    @Before
    public void before() {
        userService.saveAll(userDto, adminDto);
    }

    @After
    public void after() throws EmptyIdException {
        sessionService.removeAll();
        userService.removeOneById(userDto.getId());
        userService.removeOneById(adminDto.getId());
    }

    @Test
    public void testFindAll() {
        loadData();
        @NotNull final List<SessionDto> sessions = sessionService.findAllDto();
        Assert.assertEquals(4, sessions.size());
    }

    @Test
    public void testFindAllEntity() {
        loadData();
        @NotNull final List<Session> sessions = sessionService.findAllEntity();
        Assert.assertEquals(4, sessions.size());
    }

    @Test
    public void testFindAllDtoByUserId() throws EmptyUserIdException {
        loadData();

        @NotNull final List<SessionDto> sessions = sessionService.findAllDtoByUserId(userDto.getId());
        Assert.assertFalse(sessions.isEmpty());
        Assert.assertEquals(2, sessions.size());

        @NotNull final List<SessionDto> emptyList = sessionService.findAllDtoByUserId(unknownUserDto.getId());
        Assert.assertTrue(emptyList.isEmpty());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindAllDtoByUserIdWithoutUserId() throws EmptyUserIdException {
        sessionService.findAllDtoByUserId(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindAllDtoByUserIdWithEmptyUserId() throws EmptyUserIdException {
        sessionService.findAllDtoByUserId("");
    }

    @Test
    public void testFindAllEntityByUserId() throws EmptyUserIdException {
        loadData();

        @NotNull final List<Session> sessions = sessionService.findAllEntityByUserId(userDto.getId());
        Assert.assertFalse(sessions.isEmpty());
        Assert.assertEquals(2, sessions.size());

        @NotNull final List<Session> emptyList = sessionService.findAllEntityByUserId(unknownUserDto.getId());
        Assert.assertTrue(emptyList.isEmpty());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindAllEntityByUserIdWithoutUserId() throws EmptyUserIdException {
        sessionService.findAllEntityByUserId(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindAllEntityByUserIdWithEmptyUserId() throws EmptyUserIdException {
        sessionService.findAllEntityByUserId("");
    }

    @Test
    public void testFindOneDtoById() throws EmptyIdException {
        loadData();

        @Nullable final SessionDto session = sessionService.findOneDtoById(sessionOne.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(sessionOne.hashCode(), session.hashCode());
        Assert.assertEquals(sessionOne, session);

        @Nullable final SessionDto nullSession = sessionService.findOneDtoById(unknownSession.getId());
        Assert.assertNull(nullSession);
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneDtoByIdWithoutId() throws EmptyIdException {
        sessionService.findOneDtoById(null);
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneDtoByIdWithEmptyId() throws EmptyIdException {
        sessionService.findOneDtoById("");
    }

    @Test
    public void testFindOneEntityById() throws EmptyIdException {
        loadData();

        @Nullable final Session session = sessionService.findOneEntityById(sessionOne.getId());
        Assert.assertNotNull(session);

        @Nullable final Session nullSession = sessionService.findOneEntityById(unknownSession.getId());
        Assert.assertNull(nullSession);
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneEntityByIdWithoutId() throws EmptyIdException {
        sessionService.findOneEntityById(null);
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneEntityByIdWithEmptyId() throws EmptyIdException {
        sessionService.findOneEntityById("");
    }

    @Test
    public void testRemoveAll() {
        loadData();

        sessionService.removeAll();
        @NotNull final List<SessionDto> sessions = sessionService.findAllDto();
        Assert.assertTrue(sessions.isEmpty());
    }

    @Test
    public void testRemoveAllByUserId() throws EmptyUserIdException {
        loadData();

        sessionService.removeAllByUserId(userDto.getId());
        @NotNull List<SessionDto> sessions = sessionService.findAllDtoByUserId(userDto.getId());
        Assert.assertTrue(sessions.isEmpty());

        sessionService.removeAllByUserId(unknownUserDto.getId());
        sessions = sessionService.findAllDto();
        Assert.assertEquals(2, sessions.size());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveAllByUserIdWithoutUserId() throws EmptyUserIdException {
        sessionService.removeAllByUserId(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveAllByUserIdWithEmptyUserId() throws EmptyUserIdException {
        sessionService.removeAllByUserId(null);
    }

    @Test
    public void testRemoveOneById() throws EmptyIdException {
        loadData();

        sessionService.removeOneById(sessionOne.getId());
        @Nullable final SessionDto session = sessionService.findOneDtoById(sessionOne.getId());
        Assert.assertNull(session);

        sessionService.removeOneById(unknownSession.getId());
        @NotNull final List<SessionDto> sessions = sessionService.findAllDto();
        Assert.assertEquals(3, sessions.size());
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveOneByIdWithoutId() throws EmptyIdException {
        sessionService.removeOneById(null);
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveOneByIdWithEmptyId() throws EmptyIdException {
        sessionService.removeOneById("");
    }

    @Test
    public void testCheckDataAccess() throws AbstractException {
        loadData();

        @Nullable UserDto user = sessionService.checkDataAccess(userDto.getLogin(), "3333");
        Assert.assertNull(user);

        user = sessionService.checkDataAccess(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(user);
        Assert.assertEquals(user.hashCode(), userDto.hashCode());
        Assert.assertEquals(user, userDto);
    }

    @Test
    public void testCheckDataAccessWithoutLogin() throws AbstractException {
        @Nullable final UserDto user = sessionService.checkDataAccess(null, USER_PASSWORD);
        Assert.assertNull(user);
    }

    @Test
    public void testCheckDataAccessWithoutPassword() throws AbstractException {
        @Nullable final UserDto user = sessionService.checkDataAccess(userDto.getId(), null);
        Assert.assertNull(user);
    }

    @Test
    public void testCheckDataAccessWithEmptyLogin() throws AbstractException {
        @Nullable final UserDto user = sessionService.checkDataAccess("", USER_PASSWORD);
        Assert.assertNull(user);
    }

    @Test
    public void testCheckDataAccessWithEmptyPassword() throws AbstractException {
        @Nullable final UserDto user = sessionService.checkDataAccess(userDto.getId(), "");
        Assert.assertNull(user);
    }

    @Test
    public void testCheckDataAccessWithUnknownLogin() throws AbstractException {
        @Nullable final UserDto user = sessionService.checkDataAccess(unknownUserDto.getLogin(), "3333");
        Assert.assertNull(user);
    }

    @Test
    public void testOpen() throws AbstractException {
        loadData();
        @Nullable SessionDto session = sessionService.open(userDto.getLogin(), "3333");
        Assert.assertNull(session);

        session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getTimestamp());
        Assert.assertNotNull(session.getSignature());
        Assert.assertEquals(session.getUserId(), userDto.getId());
        @Nullable final SessionDto sessionInStorage = sessionService.findOneDtoById(sessionOne.getId());
        Assert.assertNotNull(sessionInStorage);
    }

    @Test
    public void testOpenWithoutLogin() throws AbstractException {
        @Nullable final SessionDto session = sessionService.open(null, USER_PASSWORD);
        Assert.assertNull(session);
    }

    @Test
    public void testOpenWithoutPassword() throws AbstractException {
        @Nullable final SessionDto session = sessionService.open(userDto.getId(), null);
        Assert.assertNull(session);
    }

    @Test
    public void testOpenWithEmptyLogin() throws AbstractException {
        @Nullable final SessionDto session = sessionService.open("", USER_PASSWORD);
        Assert.assertNull(session);
    }

    @Test
    public void testOpenWithEmptyPassword() throws AbstractException {
        @Nullable final SessionDto session = sessionService.open(userDto.getId(), "");
        Assert.assertNull(session);
    }

    @Test
    public void testOpenWithUnknownLogin() throws AbstractException {
        @Nullable final SessionDto session = sessionService.open(unknownUserDto.getLogin(), "3333");
        Assert.assertNull(session);
    }

    @Test
    public void testSing() {
        @Nullable SessionDto session = sessionService.setSignature(null);
        Assert.assertNull(session);

        unknownSession.setSignature(null);
        session = sessionService.setSignature(unknownSession);
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getSignature());
        @Nullable final SessionDto resignSession = sessionService.setSignature(unknownSession);
        Assert.assertNotNull(resignSession);
        Assert.assertEquals(session.getSignature(), resignSession.getSignature());
    }

    @Test
    public void testGetUser() throws AbstractException {
        loadData();

        @Nullable final UserDto user = sessionService.getUser(sessionOne);
        Assert.assertNotNull(user);
        Assert.assertEquals(user.hashCode(), userDto.hashCode());
        Assert.assertEquals(user, userDto);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserWithoutSession() throws AbstractException {
        sessionService.getUser(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserWithSessionWithoutUserId() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setUserId(null);
        sessionService.getUser(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserWithSessionWithoutTimestamp() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setTimestamp(null);
        sessionService.getUser(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserWithSessionWithoutSignature() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setSignature(null);
        sessionService.getUser(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserWithSessionEmptyUserId() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setUserId("");
        sessionService.getUser(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserWithSessionEmptySignature() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setSignature("");
        sessionService.getUser(session);
    }

    @Test
    public void testGetUserId() throws AbstractException {
        loadData();

        @Nullable final String userId = sessionService.getUserId(sessionOne);
        Assert.assertNotNull(userId);
        Assert.assertEquals(userId, userDto.getId());
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserIdWithoutSession() throws AbstractException {
        sessionService.getUserId(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserIdWithSessionWithoutUserId() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setUserId(null);
        sessionService.getUserId(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserIdWithSessionWithoutTimestamp() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setTimestamp(null);
        sessionService.getUserId(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserIdWithSessionWithoutSignature() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setSignature(null);
        sessionService.getUserId(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserIdWithSessionEmptyUserId() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setUserId("");
        sessionService.getUserId(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserIdWithSessionEmptySignature() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setSignature("");
        sessionService.getUserId(session);
    }

    @Test
    public void testGetListSession() throws AbstractException {
        loadData();

        @NotNull final List<SessionDto> sessions = sessionService.getListSession(sessionOne);
        Assert.assertNotNull(sessions);
        Assert.assertEquals(2, sessions.size());
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetListSessionWithoutSession() throws AbstractException {
        sessionService.getListSession(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetListSessionWithSessionWithoutUserId() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setUserId(null);
        sessionService.getListSession(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetListSessionWithSessionWithoutTimestamp() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setTimestamp(null);
        sessionService.getListSession(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetListSessionWithSessionWithoutSignature() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setSignature(null);
        sessionService.getListSession(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetListSessionWithSessionEmptyUserId() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setUserId("");
        sessionService.getListSession(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetListSessionWithSessionEmptySignature() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setSignature("");
        sessionService.getListSession(session);
    }

    @Test
    public void testClose() throws AbstractException {
        loadData();

        sessionService.close(sessionOne);
        @Nullable final SessionDto session = sessionService.findOneDtoById(sessionOne.getId());
        Assert.assertNull(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseWithoutSession() throws AbstractException {
        sessionService.close(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseWithSessionWithoutUserId() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setUserId(null);
        sessionService.close(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseWithSessionWithoutTimestamp() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setTimestamp(null);
        sessionService.close(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseWithSessionWithoutSignature() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setSignature(null);
        sessionService.close(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseWithSessionEmptyUserId() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setUserId("");
        sessionService.close(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseWithSessionEmptySignature() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setSignature("");
        sessionService.close(session);
    }

    @Test
    public void testCloseAll() throws AbstractException {
        loadData();

        sessionService.closeAll(sessionOne);
        @NotNull final List<SessionDto> sessions = sessionService.findAllDtoByUserId(userDto.getId());
        Assert.assertTrue(sessions.isEmpty());
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseAllWithoutSession() throws AbstractException {
        sessionService.closeAll(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseAllWithSessionWithoutUserId() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setUserId(null);
        sessionService.closeAll(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseAllWithSessionWithoutTimestamp() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setTimestamp(null);
        sessionService.closeAll(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseAllWithSessionWithoutSignature() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setSignature(null);
        sessionService.closeAll(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseAllWithSessionEmptyUserId() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setUserId("");
        sessionService.closeAll(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseAllWithSessionEmptySignature() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setSignature("");
        sessionService.closeAll(session);
    }

    @Test
    public void testValidate() throws AccessDeniedException {
        loadData();

        sessionService.validate(sessionOne);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithoutSession() throws AbstractException {
        sessionService.validate(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithSessionWithoutUserId() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setUserId(null);
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithSessionWithoutTimestamp() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setTimestamp(null);
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithSessionWithoutSignature() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setSignature(null);
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithSessionEmptyUserId() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setUserId("");
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithSessionEmptySignature() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setSignature("");
        sessionService.validate(session);
    }

    @Test
    public void testValidateWithRole() throws AbstractException {
        loadData();

        sessionService.validate(sessionOne, Role.USER);
        sessionService.validate(sessionThree, Role.ADMIN);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithRoleWithoutSession() throws AbstractException {
        sessionService.validate(null, Role.USER);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithRoleWithoutRole() throws AbstractException {
        sessionService.validate(sessionOne, null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithRoleWithSessionWithoutUserId() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setUserId(null);
        sessionService.validate(session, Role.USER);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithRoleWithSessionWithoutTimestamp() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setTimestamp(null);
        sessionService.validate(session, Role.USER);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithRoleWithSessionWithoutSignature() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setSignature(null);
        sessionService.validate(session, Role.USER);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithRoleWithSessionEmptyUserId() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setUserId("");
        sessionService.validate(session, Role.USER);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithRoleWithSessionEmptySignature() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setSignature("");
        sessionService.validate(session, Role.USER);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithRoleWithIncorrectRole() throws AbstractException {
        loadData();

        @Nullable final SessionDto session = sessionService.open(userDto.getLogin(), USER_PASSWORD);
        Assert.assertNotNull(session);
        session.setSignature("");
        sessionService.validate(session, Role.ADMIN);
    }

    @Test
    public void testIsValid() {
        loadData();

        Assert.assertTrue(sessionService.isValid(sessionOne));

        @Nullable SessionDto session = null;
        Assert.assertFalse(sessionService.isValid(session));

        session = sessionOne.clone();
        session.setTimestamp(null);
        Assert.assertFalse(sessionService.isValid(session));

        session = sessionOne.clone();
        session.setUserId(null);
        Assert.assertFalse(sessionService.isValid(session));

        session = sessionOne.clone();
        session.setSignature(null);
        Assert.assertFalse(sessionService.isValid(session));
    }

    @Test
    public void testSignOutByLogin() throws AbstractException {
        loadData();

        sessionService.signOutByLogin(userDto.getLogin());
        @NotNull final List<SessionDto> sessions = sessionService.findAllDtoByUserId(userDto.getId());
        Assert.assertTrue(sessions.isEmpty());
    }

    @Test(expected = EmptyLoginException.class)
    public void testSignOutByLoginWithoutLogin() throws AbstractException {
        sessionService.signOutByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testSignOutByLoginWithEmptyLogin() throws AbstractException {
        sessionService.signOutByLogin("");
    }

    @Test(expected = UnknownUserException.class)
    public void testSignOutByLoginWithUnknownLogin() throws AbstractException {
        sessionService.signOutByLogin(unknownUserDto.getLogin());
    }

}