package ru.malakhov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.AbstractDataTest;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.exception.empty.EmptyDomainException;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.api.service.*;
import ru.malakhov.tm.dto.*;
import sun.misc.BASE64Decoder;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class AdminServiceTest extends AbstractDataTest {

    @NotNull
    final ISessionService sessionService = context.getBean(SessionService.class);
    @NotNull
    final IUserService userService = context.getBean(UserService.class);
    @NotNull
    final IProjectService projectService = context.getBean(ProjectService.class);
    @NotNull
    final ITaskService taskService = context.getBean(TaskService.class);
    @NotNull
    private final IDomainService domainService = context.getBean(DomainService.class);
    @NotNull
    private final IAdminService adminService = context.getBean(AdminService.class);
    @NotNull
    private final String FILE_BINARY_PATH = "./data.binary";

    @NotNull
    private final String FILE_BASE64_PATH = "./data.base64";

    @NotNull
    private final String FILE_JSON_PATH = "./data.json";

    @NotNull
    private final String FILE_XML_PATH = "./data.xml";

    @NotNull
    private final List<UserDto> allUserInSystem = new ArrayList<>(Arrays.asList(userDto, adminDto));

    @NotNull
    private final ProjectDto projectOne = new ProjectDto(
            "Project2",
            "",
            userDto.getId()
    );

    @NotNull
    private final ProjectDto projectTwo = new ProjectDto(
            "Project3",
            null,
            adminDto.getId()
    );

    @NotNull
    private final List<ProjectDto> allProjectInSystem = new ArrayList<>(Arrays.asList(projectOne, projectTwo));

    @NotNull
    private final TaskDto taskOne = new TaskDto(
            "Task2",
            null,
            userDto.getId()
    );

    @NotNull
    private final TaskDto taskTwo = new TaskDto(
            "Task3",
            null,
            adminDto.getId()
    );

    @NotNull
    private final List<TaskDto> allTaskInSystem = new ArrayList<>(Arrays.asList(taskOne, taskTwo));

    @NotNull
    private final SessionDto sessionOne = sessionService.setSignature(
            new SessionDto(
                    System.currentTimeMillis(),
                    userDto.getId(),
                    null
            )
    );

    @NotNull
    private final SessionDto sessionTwo = sessionService.setSignature(
            new SessionDto(
                    System.currentTimeMillis(),
                    adminDto.getId(),
                    null
            )
    );

    @NotNull
    private final List<SessionDto> allSessionInSystem = new ArrayList<>(Arrays.asList(sessionOne, sessionTwo));

    public AdminServiceTest() throws Exception {
        super();
    }

    @NotNull
    private Domain loadData() throws EmptyDomainException {
        @NotNull final Domain domain = new Domain();
        domain.setUsers(allUserInSystem);
        domain.setProjects(allProjectInSystem);
        domain.setTasks(allTaskInSystem);
        domain.setSessions(allSessionInSystem);
        domainService.loadData(domain);
        return domain;
    }

    private void checkLoadedData() throws EmptyIdException {
        @NotNull final List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertEquals(2, projects.size());
        for (@NotNull final ProjectDto project : projects)
            Assert.assertTrue(allProjectInSystem.contains(project));

        @NotNull final List<TaskDto> tasks = taskService.findAllDto();
        Assert.assertEquals(2, tasks.size());
        for (@NotNull final TaskDto task : tasks)
            Assert.assertTrue(allTaskInSystem.contains(task));

        @NotNull final List<SessionDto> sessions = sessionService.findAllDto();
        Assert.assertEquals(2, sessions.size());
        for (@NotNull final SessionDto session : sessions)
            Assert.assertTrue(allSessionInSystem.contains(session));

        @NotNull final List<UserDto> users = userService.findAllDto();
        Assert.assertEquals(4, users.size());
    }

    private void checkLoadedData(@NotNull final Domain domain) throws EmptyIdException {
        @NotNull final List<ProjectDto> projects = domain.getProjects();
        Assert.assertEquals(2, projects.size());
        for (@NotNull final ProjectDto project : projects)
            Assert.assertTrue(allProjectInSystem.contains(project));

        @NotNull final List<TaskDto> tasks = domain.getTasks();
        Assert.assertEquals(2, tasks.size());
        for (@NotNull final TaskDto task : tasks)
            Assert.assertTrue(allTaskInSystem.contains(task));

        @NotNull final List<SessionDto> sessions = domain.getSessions();
        Assert.assertEquals(2, sessions.size());
        for (@NotNull final SessionDto session : sessions)
            Assert.assertTrue(allSessionInSystem.contains(session));

        @NotNull final List<UserDto> users = domain.getUsers();
        Assert.assertEquals(4, users.size());
    }

    public void clearApplicationData() throws EmptyIdException {
        sessionService.removeAll();
        taskService.removeAll();
        projectService.removeAll();
        userService.removeOneById(userDto.getId());
        userService.removeOneById(userDto.getId());
    }

    @After
    public void after() throws Exception {
        clearApplicationData();
        @NotNull File file = new File(FILE_XML_PATH);
        Files.deleteIfExists(file.toPath());
        file = new File(FILE_JSON_PATH);
        Files.deleteIfExists(file.toPath());
        file = new File(FILE_BASE64_PATH);
        Files.deleteIfExists(file.toPath());
        file = new File(FILE_BINARY_PATH);
        Files.deleteIfExists(file.toPath());
    }

    @Test
    public void testSaveDataBinary() throws Exception {
        @NotNull final Domain domain = loadData();

        adminService.saveDataBinary();
        @NotNull final File file = new File(FILE_BINARY_PATH);
        Assert.assertTrue(file.exists());
        try (
                @NotNull final FileInputStream fileInputStream = new FileInputStream(file);
                @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)
        ) {
            @NotNull final Domain domainInFile = (Domain) objectInputStream.readObject();
            checkLoadedData(domainInFile);
        }
        adminService.clearDataBinary();
    }

    @Test
    public void testLoadDataBinary() throws Exception {
        loadData();
        adminService.saveDataBinary();
        clearApplicationData();

        adminService.loadDataBinary();
        checkLoadedData();
    }

    @Test(expected = FileNotFoundException.class)
    public void testLoadDataBinaryWithoutDataFile() throws Exception {
        adminService.loadDataBinary();
    }

    @Test
    public void testClearDataBinary() throws IOException {
        @NotNull final File file = new File(FILE_BINARY_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        Assert.assertTrue(file.exists());
        adminService.clearDataBinary();
        Assert.assertFalse(file.exists());
    }

    @Test
    public void testSaveDataBase64() throws Exception {
        @NotNull final Domain domain = loadData();

        adminService.saveDataBase64();
        @NotNull final File file = new File(FILE_BASE64_PATH);
        Assert.assertTrue(file.exists());
        final byte[] bytes64;
        try (@NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BASE64_PATH)) {
            bytes64 = new BASE64Decoder().decodeBuffer(fileInputStream);
        }
        try (
                @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes64);
                @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream)
        ) {
            @NotNull final Domain domainInFile = (Domain) objectInputStream.readObject();
            checkLoadedData(domainInFile);
        }
        adminService.clearDataBase64();
    }

    @Test
    public void testLoadDataBase64() throws Exception {
        loadData();
        adminService.saveDataBase64();
        clearApplicationData();

        adminService.loadDataBase64();
        checkLoadedData();
    }

    @Test(expected = FileNotFoundException.class)
    public void testLoadDataBase64WithoutDataFile() throws Exception {
        adminService.loadDataBase64();
    }

    @Test
    public void testClearDataBase64() throws IOException {
        @NotNull final File file = new File(FILE_BASE64_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        Assert.assertTrue(file.exists());
        adminService.clearDataBase64();
        Assert.assertFalse(file.exists());
    }

    @Test
    public void testSaveDataJson() throws Exception {
        @NotNull final Domain domain = loadData();

        adminService.saveDataJson();
        @NotNull final File file = new File(FILE_JSON_PATH);
        Assert.assertTrue(file.exists());
        try (@NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_JSON_PATH)) {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final Domain domainInFile = objectMapper.readValue(fileInputStream, Domain.class);
            checkLoadedData(domainInFile);
        }
        adminService.clearDataJson();
    }

    @Test
    public void testLoadDataJson() throws Exception {
        loadData();
        adminService.saveDataJson();
        clearApplicationData();

        adminService.loadDataJson();
        checkLoadedData();
    }

    @Test(expected = FileNotFoundException.class)
    public void testLoadDataJsonWithoutDataFile() throws Exception {
        adminService.loadDataJson();
    }

    @Test
    public void testClearDataJson() throws IOException {
        @NotNull final File file = new File(FILE_JSON_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        Assert.assertTrue(file.exists());
        adminService.clearDataJson();
        Assert.assertFalse(file.exists());
    }

    @Test
    public void testSaveDataXml() throws Exception {
        @NotNull final Domain domain = loadData();

        adminService.saveDataXml();
        @NotNull final File file = new File(FILE_XML_PATH);
        Assert.assertTrue(file.exists());
        try (@NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_XML_PATH)) {
            @NotNull final ObjectMapper objectMapper = new XmlMapper();
            @NotNull final Domain domainInFile = objectMapper.readValue(fileInputStream, Domain.class);
            checkLoadedData(domainInFile);
        }
        adminService.clearDataXml();
    }

    @Test
    public void testLoadDataXml() throws Exception {
        loadData();
        adminService.saveDataXml();
        clearApplicationData();

        adminService.loadDataXml();
        checkLoadedData();
    }

    @Test(expected = FileNotFoundException.class)
    public void testLoadDataXmlWithoutDataFile() throws Exception {
        adminService.loadDataXml();
    }

    @Test
    public void testClearDataXml() throws IOException {
        @NotNull final File file = new File(FILE_XML_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        Assert.assertTrue(file.exists());
        adminService.clearDataXml();
        Assert.assertFalse(file.exists());
    }

}