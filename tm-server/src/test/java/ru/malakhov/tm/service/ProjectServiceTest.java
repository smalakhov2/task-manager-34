package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.AbstractDataTest;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.dto.ProjectDto;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.empty.EmptyNameException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;
import ru.malakhov.tm.exception.system.IndexIncorrectException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class ProjectServiceTest extends AbstractDataTest {

    @NotNull
    final ProjectDto unknownProject = new ProjectDto(
            "Unknown",
            null,
            unknownUserDto.getId()
    );
    @NotNull
    private final IProjectService projectService = context.getBean(ProjectService.class);
    @NotNull
    private final IUserService userService = context.getBean(UserService.class);
    @NotNull
    private final ProjectDto projectOne = new ProjectDto(
            "Project1",
            null,
            userDto.getId());
    @NotNull
    private final ProjectDto projectTwo = new ProjectDto(
            "Project2",
            null,
            userDto.getId()
    );
    @NotNull
    private final ProjectDto projectThree = new ProjectDto(
            "Project3",
            null,
            adminDto.getId()
    );
    @NotNull
    private final ProjectDto projectFour = new ProjectDto(
            "Project4",
            null,
            adminDto.getId()
    );
    @NotNull
    private final List<ProjectDto> userProjects = new ArrayList<>(Arrays.asList(projectOne, projectTwo));

    @NotNull
    private final List<ProjectDto> adminProjects = new ArrayList<>(Arrays.asList(projectThree, projectFour));

    public ProjectServiceTest() throws Exception {
        super();
    }

    private void loadData() {
        projectService.saveAll(userProjects);
        projectService.saveAll(adminProjects);
    }

    @Before
    public void before() {
        userService.saveAll(userDto, adminDto);
    }

    @After
    public void after() throws EmptyIdException {
        projectService.removeAll();
        userService.removeOneById(userDto.getId());
        userService.removeOneById(adminDto.getId());
    }

    @Test
    public void testCreate() throws AbstractException {
        @NotNull final String userId = projectOne.getUserId();
        @NotNull final String projectName = projectOne.getName();

        projectService.create(userId, projectName);
        @Nullable final ProjectDto project = projectService.findOneDtoByName(userId, projectName);
        Assert.assertNotNull(project);
        Assert.assertEquals(userId, project.getUserId());
        Assert.assertEquals(projectName, project.getName());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testCreateWithoutUserId() throws AbstractException {
        projectService.create(null, projectOne.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testCreateWithoutName() throws AbstractException {
        projectService.create(projectOne.getUserId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testCreateWithEmptyUserId() throws AbstractException {
        projectService.create("", projectOne.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testCreateWithEmptyName() throws AbstractException {
        projectService.create(projectOne.getUserId(), "");
    }

    @Test
    public void testCreateWithDescription() throws AbstractException {
        @NotNull final String userId = projectOne.getUserId();
        @NotNull final String projectName = projectOne.getName();
        @Nullable final String projectDescription = projectOne.getDescription();

        projectService.create(userId, projectName, projectDescription);
        @Nullable final ProjectDto project = projectService.findOneDtoByName(userId, projectName);
        Assert.assertNotNull(project);
        Assert.assertEquals(userId, project.getUserId());
        Assert.assertEquals(projectName, project.getName());
        Assert.assertEquals(projectDescription, project.getDescription());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testCreateWithDescriptionWithoutUserId() throws AbstractException {
        projectService.create(null, projectOne.getName(), projectOne.getDescription());
    }

    @Test(expected = EmptyNameException.class)
    public void testCreateWithDescriptionWithoutName() throws AbstractException {
        projectService.create(projectOne.getUserId(), null, projectOne.getDescription());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testCreateWithDescriptionEmptyUserId() throws AbstractException {
        projectService.create("", projectOne.getName(), projectOne.getDescription());
    }

    @Test(expected = EmptyNameException.class)
    public void testCreateWithDescriptionEmptyName() throws AbstractException {
        projectService.create(projectOne.getUserId(), "", projectOne.getDescription());
    }

    @Test
    public void testFindAll() {
        loadData();
        @NotNull final List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertEquals(4, projects.size());
    }

    @Test
    public void testFindAllEntity() {
        loadData();
        @NotNull final List<Project> projects = projectService.findAllEntity();
        Assert.assertEquals(4, projects.size());
    }

    @Test
    public void testFindAllDtoByUserId() throws EmptyUserIdException {
        loadData();

        @NotNull final List<ProjectDto> projects = projectService.findAllDtoByUserId(userDto.getId());
        Assert.assertFalse(projects.isEmpty());
        Assert.assertEquals(2, projects.size());

        @NotNull final List<ProjectDto> emptyList = projectService.findAllDtoByUserId(unknownUserDto.getId());
        Assert.assertTrue(emptyList.isEmpty());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindAllDtoByUserIdWithoutUserId() throws EmptyUserIdException {
        projectService.findAllDtoByUserId(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindAllDtoByUserIdWithEmptyUserId() throws EmptyUserIdException {
        projectService.findAllDtoByUserId("");
    }

    @Test
    public void testFindAllEntityByUserId() throws EmptyUserIdException {
        loadData();

        @NotNull final List<Project> projects = projectService.findAllEntityByUserId(userDto.getId());
        Assert.assertFalse(projects.isEmpty());
        Assert.assertEquals(2, projects.size());

        @NotNull final List<Project> emptyList = projectService.findAllEntityByUserId(unknownUserDto.getId());
        Assert.assertTrue(emptyList.isEmpty());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindAllEntityByUserIdWithoutUserId() throws EmptyUserIdException {
        projectService.findAllEntityByUserId(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindAllEntityByUserIdWithEmptyUserId() throws EmptyUserIdException {
        projectService.findAllEntityByUserId("");
    }

    @Test
    public void testFindOneDtoById() throws EmptyIdException {
        loadData();

        @Nullable final ProjectDto project = projectService.findOneDtoById(projectOne.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(projectOne.hashCode(), project.hashCode());
        Assert.assertEquals(projectOne, project);

        @Nullable final ProjectDto nullProject = projectService.findOneDtoById(unknownProject.getId());
        Assert.assertNull(nullProject);
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneDtoByIdWithoutId() throws EmptyIdException {
        projectService.findOneDtoById(null);
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneDtoByIdWithEmptyId() throws EmptyIdException {
        projectService.findOneDtoById("");
    }

    @Test
    public void testFindOneEntityById() throws EmptyIdException {
        loadData();

        @Nullable final Project project = projectService.findOneEntityByIdAndUserId(projectOne.getId());
        Assert.assertNotNull(project);

        @Nullable final Project nullProject = projectService.findOneEntityByIdAndUserId(unknownProject.getId());
        Assert.assertNull(nullProject);
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneEntityByIdWithoutId() throws EmptyIdException {
        projectService.findOneEntityByIdAndUserId(null);
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneEntityByIdWithEmptyId() throws EmptyIdException {
        projectService.findOneEntityByIdAndUserId("");
    }

    @Test
    public void testFindOneDtoByIdAndUserId() throws AbstractException {
        loadData();

        @Nullable final ProjectDto project =
                projectService.findOneDtoByIdAndUserId(projectOne.getId(), userDto.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(projectOne.hashCode(), project.hashCode());
        Assert.assertEquals(projectOne, project);

        @Nullable ProjectDto nullProject = projectService.findOneDtoByIdAndUserId(unknownUserDto.getId(), unknownProject.getId());
        Assert.assertNull(nullProject);

        nullProject = projectService.findOneDtoByIdAndUserId(userDto.getId(), unknownProject.getId());
        Assert.assertNull(nullProject);

        nullProject = projectService.findOneDtoByIdAndUserId(unknownProject.getId(), projectOne.getId());
        Assert.assertNull(nullProject);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneDtoByIdAndUserIdWithoutUserId() throws AbstractException {
        projectService.findOneDtoByIdAndUserId(null, projectOne.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneDtoByIdAndUserIdWithoutId() throws AbstractException {
        projectService.findOneDtoByIdAndUserId(userDto.getId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneDtoByIdAndUserIdWithEmptyUserId() throws AbstractException {
        projectService.findOneDtoByIdAndUserId("", projectOne.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneDtoByIdAndUserIdWithEmptyId() throws AbstractException {
        projectService.findOneDtoByIdAndUserId(userDto.getId(), "");
    }

    @Test
    public void testFindOneEntityByIdAndUserId() throws AbstractException {
        loadData();

        @Nullable final Project project = projectService.findOneEntityByIdAndUserId(userDto.getId(), projectOne.getId());
        Assert.assertNotNull(project);

        @Nullable Project nullProject = projectService.findOneEntityByIdAndUserId(unknownUserDto.getId(), unknownProject.getId());
        Assert.assertNull(nullProject);

        nullProject = projectService.findOneEntityByIdAndUserId(userDto.getId(), unknownProject.getId());
        Assert.assertNull(nullProject);

        nullProject = projectService.findOneEntityByIdAndUserId(unknownUserDto.getId(), projectOne.getId());
        Assert.assertNull(nullProject);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneEntityByIdAndUserIdWithoutUserId() throws AbstractException {
        projectService.findOneEntityByIdAndUserId(null, projectOne.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneEntityByIdAndUserIdWithoutId() throws AbstractException {
        projectService.findOneEntityByIdAndUserId(userDto.getId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneEntityByIdAndUserIdWithEmptyUserId() throws AbstractException {
        projectService.findOneEntityByIdAndUserId("", projectOne.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testFindOneEntityByIdAndUserIdWithEmptyId() throws AbstractException {
        projectService.findOneEntityByIdAndUserId(userDto.getId(), "");
    }

    @Test
    public void testFindOneDtoByIndex() throws AbstractException {
        loadData();

        @Nullable final ProjectDto project = projectService.findOneDtoByIndex(userDto.getId(), 0);
        Assert.assertNotNull(project);
        Assert.assertEquals(projectOne.hashCode(), project.hashCode());
        Assert.assertEquals(projectOne, project);

        @Nullable ProjectDto nullProject = projectService.findOneDtoByIndex(unknownProject.getId(), 6);
        Assert.assertNull(nullProject);

        nullProject = projectService.findOneDtoByIndex(userDto.getId(), 6);
        Assert.assertNull(nullProject);

        nullProject = projectService.findOneDtoByIndex(unknownUserDto.getId(), 0);
        Assert.assertNull(nullProject);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneDtoByIndexWithoutUserId() throws AbstractException {
        projectService.findOneDtoByIndex(null, 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindOneDtoByIndexWithoutIndex() throws AbstractException {
        projectService.findOneDtoByIndex(userDto.getId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneDtoByIndexWithEmptyUserId() throws AbstractException {
        projectService.findOneDtoByIndex("", 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindOneDtoByIndexWithIncorrectIndex() throws AbstractException {
        projectService.findOneDtoByIndex(userDto.getId(), -3);
    }

    @Test
    public void testFindOneEntityByIndex() throws AbstractException {
        loadData();

        @Nullable final Project project = projectService.findOneEntityByIndex(userDto.getId(), 0);
        Assert.assertNotNull(project);

        @Nullable Project nullProject = projectService.findOneEntityByIndex(unknownProject.getId(), 6);
        Assert.assertNull(nullProject);

        nullProject = projectService.findOneEntityByIndex(userDto.getId(), 6);
        Assert.assertNull(nullProject);

        nullProject = projectService.findOneEntityByIndex(unknownUserDto.getId(), 0);
        Assert.assertNull(nullProject);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneEntityByIndexWithoutUserId() throws AbstractException {
        projectService.findOneEntityByIndex(null, 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindOneEntityByIndexWithoutIndex() throws AbstractException {
        projectService.findOneEntityByIndex(userDto.getId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneEntityByIndexWithEmptyUserId() throws AbstractException {
        projectService.findOneEntityByIndex("", 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindOneEntityByIndexWithIncorrectIndex() throws AbstractException {
        projectService.findOneEntityByIndex(userDto.getId(), -3);
    }

    @Test
    public void testFindOneDtoByName() throws AbstractException {
        loadData();

        @Nullable final ProjectDto project = projectService.findOneDtoByName(userDto.getId(), projectOne.getName());
        Assert.assertNotNull(project);
        Assert.assertEquals(projectOne.hashCode(), project.hashCode());
        Assert.assertEquals(projectOne, project);

        @Nullable ProjectDto nullProject = projectService.findOneDtoByName(unknownProject.getId(), unknownProject.getName());
        Assert.assertNull(nullProject);

        nullProject = projectService.findOneDtoByName(userDto.getId(), unknownProject.getName());
        Assert.assertNull(nullProject);

        nullProject = projectService.findOneDtoByName(unknownUserDto.getId(), projectOne.getName());
        Assert.assertNull(nullProject);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneDtoByNameWithoutUserId() throws AbstractException {
        projectService.findOneDtoByName(null, projectOne.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testFindOneDtoByNameWithoutName() throws AbstractException {
        projectService.findOneDtoByName(userDto.getId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneDtoByNameWithEmptyUserId() throws AbstractException {
        projectService.findOneDtoByName("", projectOne.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testFindOneDtoByNameWithEmptyName() throws AbstractException {
        projectService.findOneDtoByName(userDto.getId(), "");
    }

    @Test
    public void testFindOneEntityByName() throws AbstractException {
        loadData();

        @Nullable final Project project = projectService.findOneEntityByName(userDto.getId(), projectOne.getName());
        Assert.assertNotNull(project);

        @Nullable Project nullProject = projectService.findOneEntityByName(unknownProject.getId(), unknownProject.getName());
        Assert.assertNull(nullProject);

        nullProject = projectService.findOneEntityByName(userDto.getId(), unknownProject.getName());
        Assert.assertNull(nullProject);

        nullProject = projectService.findOneEntityByName(unknownUserDto.getId(), projectOne.getName());
        Assert.assertNull(nullProject);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneEntityByNameWithoutUserId() throws AbstractException {
        projectService.findOneEntityByName(null, projectOne.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testFindOneEntityByNameWithoutName() throws AbstractException {
        projectService.findOneEntityByName(userDto.getId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindOneEntityByNameWithEmptyUserId() throws AbstractException {
        projectService.findOneEntityByName("", projectOne.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testFindOneEntityByNameWithEmptyName() throws AbstractException {
        projectService.findOneEntityByName(userDto.getId(), "");
    }

    @Test
    public void testRemoveAll() {
        loadData();

        projectService.removeAll();
        @NotNull final List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertTrue(projects.isEmpty());
    }

    @Test
    public void testRemoveAllByUserId() throws EmptyUserIdException {
        loadData();

        projectService.removeAllByUserId(userDto.getId());
        @NotNull List<ProjectDto> projects = projectService.findAllDtoByUserId(userDto.getId());
        Assert.assertTrue(projects.isEmpty());

        projectService.removeAllByUserId(unknownUserDto.getId());
        projects = projectService.findAllDto();
        Assert.assertEquals(2, projects.size());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveAllByUserIdWithoutUserId() throws EmptyUserIdException {
        projectService.removeAllByUserId(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveAllByUserIdWithEmptyUserId() throws EmptyUserIdException {
        projectService.removeAllByUserId("");
    }

    @Test
    public void testRemoveOneById() throws EmptyIdException {
        loadData();

        projectService.removeOneById(projectOne.getId());
        @Nullable final ProjectDto project = projectService.findOneDtoById(projectOne.getId());
        Assert.assertNull(project);

        projectService.removeOneById(unknownProject.getId());
        @NotNull final List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertEquals(3, projects.size());
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveOneByIdWithoutId() throws EmptyIdException {
        projectService.removeOneById(null);
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveOneByIdWithEmptyId() throws EmptyIdException {
        projectService.removeOneById("");
    }

    @Test
    public void testRemoveOneByIdAndUserId() throws AbstractException {
        loadData();

        projectService.removeOneByIdAndUserId(projectOne.getId(), userDto.getId());
        @Nullable final ProjectDto project = projectService.findOneDtoById(projectOne.getId());
        Assert.assertNull(project);

        projectService.removeOneByIdAndUserId(unknownUserDto.getId(), unknownProject.getId());
        @NotNull List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertEquals(3, projects.size());

        projectService.removeOneByIdAndUserId(userDto.getId(), unknownProject.getId());
        projects = projectService.findAllDto();
        Assert.assertEquals(3, projects.size());

        projectService.removeOneByIdAndUserId(unknownUserDto.getId(), projectOne.getId());
        projects = projectService.findAllDto();
        Assert.assertEquals(3, projects.size());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveOneByIdAndUserIdIdWithoutUserId() throws AbstractException {
        projectService.removeOneByIdAndUserId(null, projectOne.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveOneEntityByIdAndUserIdWithoutId() throws AbstractException {
        projectService.removeOneByIdAndUserId(userDto.getId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveOneEntityByIdAndUserIdWithEmptyUserId() throws AbstractException {
        projectService.removeOneByIdAndUserId("", projectOne.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveOneEntityByIdAndUserIdWithEmptyId() throws AbstractException {
        projectService.removeOneByIdAndUserId(userDto.getId(), "");
    }

    @Test
    public void testRemoveOneByIndex() throws AbstractException {
        loadData();

        projectService.removeOneByIndex(userDto.getId(), 0);
        @Nullable final ProjectDto project = projectService.findOneDtoById(projectOne.getId());
        Assert.assertNull(project);

        projectService.removeOneByIndex(unknownUserDto.getId(), 6);
        @NotNull List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertEquals(3, projects.size());

        projectService.removeOneByIndex(userDto.getId(), 6);
        projects = projectService.findAllDto();
        Assert.assertEquals(3, projects.size());

        projectService.removeOneByIndex(unknownUserDto.getId(), 0);
        projects = projectService.findAllDto();
        Assert.assertEquals(3, projects.size());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveOneEntityByIndexWithoutUserId() throws AbstractException {
        projectService.removeOneByIndex(null, 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveOneEntityByIndexWithoutIndex() throws AbstractException {
        projectService.removeOneByIndex(userDto.getId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveOneEntityByIndexWithEmptyUserId() throws AbstractException {
        projectService.removeOneByIndex("", 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveOneEntityByIndexWithIncorrectIndex() throws AbstractException {
        projectService.removeOneByIndex(userDto.getId(), -3);
    }

    @Test
    public void testRemoveOneByName() throws AbstractException {
        loadData();

        projectService.removeOneByName(userDto.getId(), projectOne.getName());
        @Nullable final ProjectDto project = projectService.findOneDtoById(projectOne.getId());
        Assert.assertNull(project);

        projectService.removeOneByName(unknownUserDto.getId(), unknownProject.getName());
        @NotNull List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertEquals(3, projects.size());

        projectService.removeOneByName(userDto.getId(), unknownProject.getName());
        projects = projectService.findAllDto();
        Assert.assertEquals(3, projects.size());

        projectService.removeOneByName(unknownUserDto.getId(), projectOne.getName());
        projects = projectService.findAllDto();
        Assert.assertEquals(3, projects.size());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveOneEntityByNameWithoutUserId() throws AbstractException {
        projectService.removeOneByName(null, projectOne.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testRemoveOneEntityByNameWithoutName() throws AbstractException {
        projectService.removeOneByName(userDto.getId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveOneEntityByNameWithEmptyUserId() throws AbstractException {
        projectService.removeOneByName("", projectOne.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testRemoveOneEntityByNameWithEmptyName() throws AbstractException {
        projectService.removeOneByName(userDto.getId(), "");
    }

    @Test
    public void testUpdateById() throws AbstractException {
        loadData();
        @NotNull final String unknownUserId = unknownProject.getUserId();
        @NotNull final String unknownId = unknownProject.getId();
        @NotNull final String userId = projectOne.getUserId();
        @NotNull final String id = projectOne.getId();
        @NotNull final String newName = "New name";
        @NotNull final String newDescription = "New description";

        projectService.updateProjectById(
                userId,
                id,
                newName,
                newDescription
        );
        @Nullable final ProjectDto project = projectService.findOneDtoById(id);
        Assert.assertNotNull(project);
        Assert.assertEquals(newName, project.getName());
        Assert.assertEquals(newDescription, project.getDescription());

        projectService.updateProjectById(
                unknownUserId,
                unknownId,
                newName,
                newDescription
        );
        @Nullable final ProjectDto nullProject = projectService.findOneDtoByIdAndUserId(unknownId, unknownUserId);
        Assert.assertNull(nullProject);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdateByIdWithoutUserId() throws AbstractException {
        projectService.updateProjectById(
                null,
                projectOne.getId(),
                projectOne.getName(),
                projectOne.getDescription()
        );
    }

    @Test(expected = EmptyIdException.class)
    public void testUpdateByIdWithoutId() throws AbstractException {
        projectService.updateProjectById(
                projectOne.getUserId(),
                null,
                projectOne.getName(),
                projectOne.getDescription()
        );
    }

    @Test(expected = EmptyNameException.class)
    public void testUpdateByIdWithoutName() throws AbstractException {
        projectService.updateProjectById(
                projectOne.getUserId(),
                projectOne.getId(),
                null,
                projectOne.getDescription()
        );
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdateByIdWithEmptyUserId() throws AbstractException {
        projectService.updateProjectById(
                "",
                projectOne.getId(),
                projectOne.getName(),
                projectOne.getDescription()
        );
    }

    @Test(expected = EmptyIdException.class)
    public void testUpdateByIdWithEmptyId() throws AbstractException {
        projectService.updateProjectById(
                projectOne.getUserId(),
                "",
                projectOne.getName(),
                projectOne.getDescription()
        );
    }

    @Test(expected = EmptyNameException.class)
    public void testUpdateByIdWithEmptyName() throws AbstractException {
        projectService.updateProjectById(
                projectOne.getUserId(),
                projectOne.getId(),
                "",
                projectOne.getDescription()
        );
    }

    @Test
    public void testUpdateByIndex() throws AbstractException {
        loadData();
        @NotNull final String unknownUserId = unknownUserDto.getId();
        @NotNull final String userId = userDto.getId();
        final int index = 0;
        @NotNull final String newName = "New name1";
        @NotNull final String newDescription = "New description1";

        projectService.updateProjectByIndex(
                userId,
                index,
                newName,
                newDescription
        );
        @Nullable final ProjectDto project = projectService.findOneDtoByIndex(userId, index);
        Assert.assertNotNull(project);
        Assert.assertEquals(newName, project.getName());
        Assert.assertEquals(newDescription, project.getDescription());

        projectService.updateProjectByIndex(
                unknownUserId,
                5,
                newName,
                newDescription
        );
        @Nullable final ProjectDto nullProject = projectService.findOneDtoByIndex(unknownUserId, 5);
        Assert.assertNull(nullProject);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdateByIndexWithoutUserId() throws AbstractException {
        loadData();
        projectService.updateProjectByIndex(
                null,
                0,
                projectOne.getName(),
                projectOne.getDescription()
        );
    }

    @Test(expected = EmptyNameException.class)
    public void testUpdateByIndexWithoutName() throws AbstractException {
        loadData();
        projectService.updateProjectByIndex(
                projectOne.getUserId(),
                0,
                null,
                projectOne.getDescription()
        );
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdateByIndexWithEmptyUserId() throws AbstractException {
        loadData();
        projectService.updateProjectByIndex(
                "",
                0,
                projectOne.getName(),
                projectOne.getDescription()
        );
    }

    @Test(expected = EmptyNameException.class)
    public void testUpdateByIndexWithEmptyName() throws AbstractException {
        loadData();
        projectService.updateProjectByIndex(
                projectOne.getUserId(),
                0,
                "",
                projectOne.getDescription()
        );
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIndexWithIndexLessRange() throws AbstractException {
        projectService.updateProjectByIndex(
                projectOne.getUserId(),
                -1,
                projectOne.getName(),
                projectOne.getDescription()
        );
    }

}