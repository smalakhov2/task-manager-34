package ru.malakhov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.malakhov.tm.api.endpoint.ITaskEndpoint;
import ru.malakhov.tm.api.service.ISessionService;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.dto.TaskDto;
import ru.malakhov.tm.dto.response.Fail;
import ru.malakhov.tm.dto.response.Result;
import ru.malakhov.tm.dto.response.Success;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Controller
@NoArgsConstructor
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Override
    @WebMethod
    public Result clearAllTask(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) {
        try {
            sessionService.validate(session, Role.ADMIN);
            taskService.removeAll();
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDto> getAllTaskList(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        sessionService.validate(session, Role.ADMIN);
        return taskService.findAllDto();
    }

    @NotNull
    @Override
    @WebMethod
    public Result createTask(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        try {
            sessionService.validate(session);
            taskService.create(session.getUserId(), name, description);
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearTaskList(@WebParam(name = "session") @Nullable final SessionDto session) {
        try {
            sessionService.validate(session);
            taskService.removeAllByUserId(session.getUserId());
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDto> getTaskList(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        sessionService.validate(session);
        return taskService.findAllDtoByUserId(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public Result removeTaskById(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "id") @Nullable final String id
    ) throws AbstractException {
        try {
            sessionService.validate(session);
            taskService.removeOneByIdAndUserId(id, session.getUserId());
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @Nullable
    @Override
    @WebMethod
    public Result removeTaskByIndex(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "index") @Nullable final Integer index
    ) throws AbstractException {
        try {
            sessionService.validate(session);
            taskService.removeOneByIndex(session.getUserId(), index);
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @Nullable
    @Override
    @WebMethod
    public Result removeTaskByName(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "name") @Nullable final String name
    ) throws AbstractException {
        try {
            sessionService.validate(session);
            taskService.removeOneByName(session.getUserId(), name);
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDto getTaskById(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "id") @Nullable final String id
    ) throws AbstractException {
        sessionService.validate(session);
        return taskService.findOneDtoByIdAndUserId(id, session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDto getTaskByIndex(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "index") @Nullable final Integer index
    ) throws AbstractException {
        sessionService.validate(session);
        return taskService.findOneDtoByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDto getTaskByName(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "name") @Nullable final String name
    ) throws AbstractException {
        sessionService.validate(session);
        return taskService.findOneDtoByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public Result updateTaskById(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        try {
            sessionService.validate(session);
            taskService.updateTaskById(
                    session.getUserId(),
                    id,
                    name,
                    description
            );
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result updateTaskByIndex(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "index") @Nullable final Integer index,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        try {
            sessionService.validate(session);
            taskService.updateTaskByIndex(
                    session.getUserId(),
                    index,
                    name,
                    description
            );
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

}