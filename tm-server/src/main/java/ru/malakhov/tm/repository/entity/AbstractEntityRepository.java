package ru.malakhov.tm.repository.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.malakhov.tm.entity.AbstractEntity;

public interface AbstractEntityRepository<E extends AbstractEntity> extends JpaRepository<E, String> {
}