package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    void init() throws Exception;

    @NotNull
    String getServerHost();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getSessionSalt();

    @NotNull
    Integer getSessionCycle();

    @NotNull
    public String getJdbcDriver();

    @NotNull
    public String getJdbcUrl();

    @NotNull
    public String getJdbcUsername();

    @NotNull
    public String getJdbcPassword();

}