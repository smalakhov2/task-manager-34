package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.UserDto;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.empty.EmptyLoginException;
import ru.malakhov.tm.repository.dto.UserDtoRepository;

import java.util.List;

public interface IUserService extends IService<UserDto, UserDtoRepository> {

    @NotNull
    List<UserDto> findAllDto();

    @NotNull
    List<User> findAllEntity();

    @Nullable
    UserDto findOneDtoById(
            @Nullable String id
    ) throws EmptyIdException;

    @Nullable
    User findOneEntityById(
            @Nullable String id
    ) throws EmptyIdException;

    @Nullable
    public UserDto findOneDtoByLogin(
            @Nullable String login
    ) throws EmptyLoginException;

    @Nullable
    User findOneEntityByLogin(
            @Nullable String login
    ) throws EmptyLoginException;

    void removeAll();

    void removeOneById(
            @Nullable String id
    ) throws EmptyIdException;

    void removeOneByLogin(
            @Nullable String login
    ) throws EmptyLoginException;

    void create(
            @Nullable String login,
            @Nullable String password
    ) throws AbstractException;

    void create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    ) throws AbstractException;

    void create(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role
    ) throws AbstractException;

    void updatePassword(
            @Nullable String userId,
            @Nullable String password,
            @Nullable String newPassword
    ) throws AbstractException;

    void updateUserInfo(
            @Nullable String userId,
            @Nullable String email,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    ) throws AbstractException;

    void lockUserByLogin(
            @Nullable String login
    ) throws AbstractException;

    void unlockUserByLogin(
            @Nullable String login
    ) throws AbstractException;

}