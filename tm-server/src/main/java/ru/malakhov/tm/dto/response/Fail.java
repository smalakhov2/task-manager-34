package ru.malakhov.tm.dto.response;

import org.jetbrains.annotations.Nullable;

public final class Fail extends Result {

    public Fail() {
        success = false;
        message = "";
    }

    public Fail(@Nullable final Exception e) {
        success = false;
        if (e == null) return;
        message = e.getMessage();
    }

}